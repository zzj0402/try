import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";

import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { NgxFileDropModule } from "ngx-file-drop";
import { DragComponent } from './drag/drag.component';
@NgModule({
  declarations: [AppComponent, DragComponent],
  imports: [BrowserModule, FormsModule, HttpClientModule, NgxFileDropModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
